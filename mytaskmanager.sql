-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2021 at 11:22 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mytaskmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `UserId` int(10) UNSIGNED NOT NULL,
  `Created_At` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`Id`, `Title`, `UserId`, `Created_At`) VALUES
(1, 'jobs', 1, '2021-12-02 17:18:11'),
(22, 'school work', 1, '2021-12-03 14:39:46'),
(23, 'gym', 1, '2021-12-03 14:52:02'),
(35, 'works', 3, '2021-12-07 13:38:34');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `UserId` int(10) UNSIGNED NOT NULL,
  `FolderId` int(10) UNSIGNED NOT NULL,
  `Is_Done` tinyint(1) NOT NULL DEFAULT 0,
  `Created_At` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`Id`, `Title`, `UserId`, `FolderId`, `Is_Done`, `Created_At`) VALUES
(1, 'learn php', 1, 1, 1, '2021-12-03 11:34:52'),
(4, 'practice body building', 1, 23, 0, '2021-12-04 13:11:18'),
(7, 'practice logic madar', 1, 22, 1, '2021-12-07 12:54:18'),
(10, 'read book', 3, 35, 0, '2021-12-07 13:38:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` int(11) UNSIGNED NOT NULL,
  `Name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `LastName` varchar(255) CHARACTER SET utf8 NOT NULL,
  `UserName` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Created_At` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `Name`, `LastName`, `UserName`, `Password`, `Email`, `Created_At`) VALUES
(1, 'amir', 'tst', 'amirprogramer', '81dc9bdb52d04dc20036dbd8313ed055', 'amir@gmail.com', '2021-12-04 15:48:52'),
(3, 'Jenny', 'Herry', 'Jenny', '81dc9bdb52d04dc20036dbd8313ed055', 'jenny@gmail.com', '2021-12-07 13:08:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
