<?php
include "bootstrap/init.php";
if (!CheckLogin()) {
    header('Location: auth.php');
    die();
}
if(isset($_GET['Logout']) && $_GET['Logout']== 'True')
Logout();
if (isset($_GET['FolderId']) && !empty($_GET['FolderId'])) {
    RemoveFolder($_GET['FolderId']);
    header('Location: index.php');
}
if (isset($_GET['TaskId']) && !empty($_GET['TaskId'])) {
    UpdateTask($_GET['TaskId']);
    header('Location: index.php');
}
if (isset($_GET['TaskIdRemove']) && !empty($_GET['TaskIdRemove'])) {
    RemoveTask($_GET['TaskIdRemove']);
    header('Location: index.php');
}
$Folders = GetFolders();
$Tasks=(isset($_GET['Category']) && !empty($_GET["Category"]))? $Tasks = GetTasks($_GET['Category']): GetTasks() ;
include "tpl/index-tpl.php";
