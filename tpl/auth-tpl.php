<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>7todo authentication</title>
    <link rel="stylesheet" href="assets/css/auth-style.css">

</head>

<body>
    <!-- partial:index.partial.html -->
    <div id="background">
        <div id="panel-box">
            <div class="panel">
                <div class="auth-form on" id="login">
                    <div id="form-title">Log In</div>
                    <form action="auth.php" method="POST">
                        <input name="Email" type="email" required="required" placeholder="Email" />
                        <input name="Password" type="password" required="required" placeholder="password" />
                        <button type="Submit" name="login">Log In</button>
                    </form>
                </div>
                <div class="auth-form" id="signup">
                    <div id="form-title">Register</div>
                    <form action="auth.php" method="POST">
                        <input name="Name" type="text" required="required" placeholder="Name" />
                        <input name="LastName" type="text" required="required" placeholder="LastName" />
                        <input name="UserName" type="text" required="required" placeholder="username" />
                        <input name="Password" type="password" required="required" placeholder="password" />
                        <input name="Email" type="email" required="required" placeholder="Email" />
                        <button name="Register" type="Submit">Sign Up</button>
                    </form>

                </div>
            </div>
            <div class="panel">
                <div id="switch">Sign Up</div>
                <div id="image-overlay"></div>
                <div id="image-side"></div>
            </div>
        </div>
    </div>
    <!-- partial -->
    <script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
    <script src="assets/js/auth-script.js"></script>

</body>

</html>