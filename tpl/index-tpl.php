<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Task manager</title>
    <link rel="stylesheet" href="assets/css/index-style.css">
    <style>
        .RemoveLink {
            text-decoration: none;
            color: #ff0000;
            float: right;
            font-weight: bold;
            cursor: pointer;
            position: relative;
            top: -28px;
            right: 25px;
        }

        .main .nav .menu ul li {
            padding-left: 0;
            width: 150px;
        }

        .AddFolderName {
            float: left;
            padding-left: 5px;
            width: 140px;
            height: 20px;
            border: 1px solid #eee;
            border-radius: 5px;
            position: relative;
            left: -2px;
        }

        .AddFolderBtn {
            float: right;
            padding-left: 5px;
            width: 25px;
            height: 21px;
            border: 1px solid #eee;
            border-radius: 5px;
            position: relative;
            background: #ccc;
            right: 14px;
            top: 1px;
            cursor: pointer;
        }

        .UpdateTask {
            text-decoration: none;
            color: #000;
        }

        .RemoveTask {
            text-decoration: none;
            background: #ff0000;
            color: #ffffff;
            width: 64px;
            float: left;
            position: relative;
            right: -280px;
            text-align: center;
            height: 29px;
            line-height: 30px;
            top: 10px;
            border-radius: 5px;
            border: 1px solid #fff;
            cursor: pointer;
        }

        .main .view .content .list ul li .info span {
            margin-right: 80px;
        }

        .Category {
            text-decoration: none;
            color: #000;
        }

        .CatActive {
            color: green;
        }

        .NewTaskName {
            width: 250px;
            position: relative;
            top: -19px;
            height: 24px;
            padding-left: 10px;
            border-radius: 3px;
            border: 1px solid #999;
        }

        .Logout {
            text-decoration: none;
            color: #fff;
            font-size: 20px;
            margin-left: 8px;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <!-- partial:index.partial.html -->
    <div class="page">
        <div class="pageHeader">
            <div class="title">Dashboard</div>
            <div class="userPanel"><i class="fa fa-chevron-down"><a class="Logout" href="?Logout=True">Logout</a></i><span class="username"><?= $_SESSION['UserInfo'][0]->Name . " " . $_SESSION['UserInfo'][0]->LastName; ?> </span><img src="assets/img/images.png" width="40" height="40" /></div>
        </div>
        <div class="main">
            <div class="nav">
                <div class="searchbox">
                    <div><i class="fa fa-search"></i>
                        <input type="search" placeholder="Search" />
                    </div>
                </div>
                <div class="menu">
                    <div class="title">Navigation</div>
                    <ul class="FolderList">
                        <li>
                            <a class="Category <?= !isset($_GET['Category']) ? 'CatActive' : '' ?>" href="?AllFolder=0">
                                <i class="fa fa-folder"></i>
                            </a>
                            All
                        </li>
                        <?php
                        foreach ($Folders as $Folder) :
                        ?>
                            <li>
                                <a class="Category <?= isset($_GET['Category']) && $_GET['Category'] == $Folder->Id ? 'CatActive' : '' ?>" href="?Category=<?= $Folder->Id ?>">
                                    <i class="fa fa-folder"></i><?= $Folder->Title ?>
                                </a>
                            </li>
                            <a href="?FolderId=<?= $Folder->Id ?>" class="RemoveLink" onclick="return confirm('Are You Sure To Delete This Item ?');">*</a>
                        <?php endforeach ?>
                    </ul>
                    <input type="text" class="AddFolderName" placeholder="Add New Folder">
                    <button class="AddFolderBtn">+</button>
                </div>
            </div>
            <div class="view">
                <div class="viewHeader">
                    <div class="title">Manage Tasks</div>
                    <div class="functions">
                        <div class="button active AddNewTaskBtn">Add New Task</div>
                        <input type="text" placeholder="Add New Task" class="NewTaskName">
                    </div>
                </div>
                <div class="content">
                    <div class="list">
                        <div class="title">
                            <?php
                            if (isset($_GET['Category'])) {
                                $FolderName = GetFolderName($_GET['Category']);
                                echo $FolderName[0]['Title'];
                            } else
                                echo "All Task";
                            ?>
                        </div>
                        <ul>
                            <?php if (empty($Tasks)) echo "<p>No Any Items</p>"; ?>
                            <?php foreach ($Tasks as $Task) : ?>
                                <li class="<?php echo ($Task->Is_Done) ? "checked" : ""; ?>">
                                    <a class="UpdateTask" href="?TaskId=<?= $Task->Id; ?>">
                                        <i class="fa <?php echo ($Task->Is_Done) ? "fa-check-square-o" : "fa-square-o"; ?>"></i>
                                    </a>
                                    <span><?= $Task->Title; ?></span>
                                    <div class="info">
                                        <a class="RemoveTask" href="?TaskIdRemove=<?= $Task->Id; ?>" onclick="return confirm('Are You Sure To Delete This Item?');">remove</a>
                                        <div class="button <?php if ($Task->Is_Done) echo "green"; ?>">
                                            <?php
                                            if ($Task->Is_Done)
                                                echo "Done";
                                            else
                                                echo "Not Done";
                                            ?>
                                        </div><span>Created At <?= $Task->Created_At; ?></span>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- partial -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="assets/js/index-script.js"></script>
    <script>
        // add new folder
        $('.AddFolderBtn').click(function() {
            var NewFolderName = $('.AddFolderName').val();
            $.ajax({
                url: 'proccess/ajax-handler.php',
                method: "post",
                data: {
                    Action: "AddNewFolder",
                    NewFolderName: NewFolderName
                },
                success: function(response) {
                    if (response != 1)
                        alert(response);
                    else
                        location.reload();
                }
            });
        });
        // add new task
        $('.AddNewTaskBtn').click(function() {
            var NewTaskName = $('.NewTaskName').val();
            $.ajax({
                url: 'proccess/ajax-handler.php',
                method: 'post',
                data: {
                    Action: 'AddNewTask',
                    NewTaskName: NewTaskName,
                    Category: <?php if (isset($_GET['Category']) && !empty($_GET['Category']))
                                    echo $_GET['Category'];
                                else
                                    echo 0;
                                ?>
                },
                success: function(response) {
                    if (response == 1)
                        location.reload();
                    else
                        alert(response);
                }
            });
        });
    </script>
</body>

</html>