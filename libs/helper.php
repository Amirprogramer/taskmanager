<?php
// get current user id
function GetCurrentUserId()
{
    return $_SESSION['UserInfo'][0]->Id;
}
// get folders
function GetFolders()
{
    global $Connection;
    $CurrentUser = GetCurrentUserId();
    $Query = "SELECT * FROM folders WHERE UserId = :UserId";
    $stmt = $Connection->prepare($Query);
    $stmt->execute(["UserId" => $CurrentUser]);
    $res = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $res;
}
// remove  folders
function RemoveFolder($FolderId)
{
    if (isset($FolderId) && !empty($FolderId) && is_numeric($FolderId)) {
        global $Connection;
        $CurrenUser = GetCurrentUserId();
        $Query = "DELETE FROM folders WHERE UserId =:UserId AND Id =:Id";
        $stmt = $Connection->prepare($Query);
        $stmt->execute(["UserId" => $CurrenUser, "Id" => $FolderId]);
    }
}
// add new folder
function AddNewFolder($FolderName)
{
    global $Connection;
    $CurrentUser = GetCurrentUserId();
    $Query = "INSERT INTO folders (Title,UserId) VALUES (:Title,:UserId)";
    $stmt = $Connection->prepare($Query);
    $stmt->execute(["Title" => $FolderName, "UserId" => $CurrentUser]);
    return $stmt->rowCount();
}
// get all task
function GetTasks($FolderId=null)
{
    global $Connection;
    $CurrentUser = GetCurrentUserId();
    if(isset($FolderId) && !empty($FolderId)){
        $Query = "SELECT * FROM tasks WHERE UserId = :UserId and FolderId =:FolderId";
        $stmt = $Connection->prepare($Query);
        $stmt->execute(["UserId" => $CurrentUser , "FolderId" => $FolderId]);
        $res = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
 else{
        $Query = "SELECT * FROM tasks WHERE UserId = :UserId";
        $stmt = $Connection->prepare($Query);
        $stmt->execute(["UserId" => $CurrentUser]);
        $res = $stmt->fetchAll(PDO::FETCH_OBJ);
 }
 return $res;
}
// update task
function UpdateTask($TaskId)
{
    global $Connection;
    $CurrentUser = GetCurrentUserId();
    $Query = "UPDATE tasks SET Is_Done = 1-Is_Done WHERE Id = :TaskId and UserId = :UserId";
    $stmt = $Connection->prepare($Query);
    $stmt->execute(["TaskId" => $TaskId, "UserId" => $CurrentUser]);
}
// remove task
function RemoveTask($TaskId)
{
    global $Connection;
    $CurrentUser = GetCurrentUserId();
    $Query = "DELETE FROM tasks WHERE Id =:TaskId and UserId =:UserId";
    $stmt = $Connection->prepare($Query);
    $stmt->execute(["TaskId" => $TaskId, "UserId" => $CurrentUser]);
}
// get folder name
function GetFolderName($FolderId){
global $Connection;
$CurrentUser = GetCurrentUserId();
$Query = "SELECT Title FROM folders WHERE Id =:Id and UserId=:UserId" ;
$stmt = $Connection->prepare($Query);
$stmt->execute(["Id" => $FolderId , "UserId" => $CurrentUser]);
return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
// add new task
function AddNewTask($FolderId,$TaskName){
global $Connection;
$CurrentUser = GetCurrentUserId();
$Query = "INSERT INTO tasks (Title,FolderId,UserId) VALUES (:Title,:FolderId,:UserId)";
$stmt = $Connection->prepare($Query);
$stmt->execute(["Title" => $TaskName , "FolderId" => $FolderId , "UserId" => $CurrentUser]);
return $stmt->rowCount();
}
// check login
function CheckLogin(){
    if(isset($_SESSION['UserInfo']) && !empty($_SESSION['UserInfo']))
    return true;
    else
    return false;
}
// register user
function Register($Name,$LastName,$UserName,$Password,$Email){
    global $Connection;
    $Password = md5($Password);
    $Query = "INSERT INTO users (Name,LastName,UserName,Password,Email) VALUES (:Name,:LastName,:UserName,:Password,:Email)";
    $stmt = $Connection->prepare($Query);
    $stmt->execute(
        ['Name' => $Name, 'LastName' => $LastName, 'UserName' => $UserName, 'Password' => $Password, 'Email' => $Email]
    );
    return $stmt->rowCount();
}
// login user
function Login($Email,$Password){
global $Connection;
$HashPassword = md5($Password);
$Query = 'SELECT * FROM users WHERE Email =:Email and Password =:Password';
$stmt = $Connection->prepare($Query);
$stmt->execute([
    'Email' => $Email , 'Password' => $HashPassword
]);
$res = $stmt->rowCount();
if($res == 1)
return $stmt->fetchAll(PDO::FETCH_OBJ);
else
return false;
}
// Logout
function Logout(){
    session_destroy();
}